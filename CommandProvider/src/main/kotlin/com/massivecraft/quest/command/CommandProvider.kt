package com.massivecraft.quest.command

interface CommandProvider {
    fun toCommandStrings(): List<String>
}

// For receiving
interface CommandProviderReceiver {
    fun cmd(vararg cmds: String)
    fun cmd(cmds: Collection<String>)
    fun cmd(vararg cmds: CommandProvider)
}

class CommandProviderFixed(val cmds: Collection<String>): CommandProvider {
    override fun toCommandStrings(): List<String> {
        return cmds.toList()
    }
}

class CommandProviderList: ArrayList<CommandProvider>(), CommandProviderReceiver {
    override fun cmd(cmds: Collection<String>) {
        add(CommandProviderFixed(cmds))
    }

    override fun cmd(vararg cmds: String) {
        add(CommandProviderFixed(cmds.toList()))
    }

    override fun cmd(vararg cmds: CommandProvider) {
        addAll(cmds)
    }

    fun getCommands(): List<String> {
        return flatMap { it.toCommandStrings().toList() }
    }
}
