import com.massivecraft.questwriter.Region
import com.massivecraft.questwriter.dsl.*
import com.massivecraft.questwriter.dsl.rule.doCmds
import com.massivecraft.questwriter.dsl.rule.onEnterRegion
import com.massivecraft.questwriter.dsl.rule.onExitRegion
import com.massivecraft.questwriter.quest.nodes
import com.massivecraft.questwriter.quest.quest

val quest = quest {
    val regionDropSection = Region("event", "event-hungergames-start")
    nodes {
        node {
            id = "event.hungergames.start"
            visible = false
            times = 1
            cooldown = "2s"
            toFind {
                onEnterRegion(regionDropSection)
            }
            toAdvance {
                onExitRegion(regionDropSection)
            }
            onComplete {
                doCmds("/sudo {p} /t set fallimmune")
            }
        }
    }
}
quest.implementation().forEach { println(it) }
println("\n\n\n")
quest.backout().forEach { println(it) }
