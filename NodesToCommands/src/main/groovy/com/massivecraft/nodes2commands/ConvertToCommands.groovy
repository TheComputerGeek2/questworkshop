/**
 * Just pass the json export file as the argument for running the generator, it will write the nodes to stdout
 */
package com.massivecraft.nodes2commands

import groovy.json.JsonSlurper

def inputFile = new File(args[0])

static String stripMongoCrap(String raw) {
    return raw.replaceAll(~/NumberLong\((\d+)\)/, '$1')
}

String cleaned = stripMongoCrap(inputFile.text)
JsonSlurper slurper = new JsonSlurper()
def parsed = slurper.parseText(cleaned)
def list = parsed as ArrayList

for (def node: list) {
    // create node
    println("/q new $node._id")

    // Category
    boolean nCategory = node.category
    if (nCategory) println("/q e category true")

    // Visible
    boolean nVisible = node.visible
    if (!nVisible) println("/q e visible false")

    // Name
    if (node.name != null) println("/q e name $node.name")

    // Description
    if (node.desc != null) println("/q e desc $node.desc")

    // Cooldown
    if (node.cooldown != null) println("/q e cooldown $node.cooldown")

    // Times
    println("/q e times $node.times")

    // ToFind
    def nToFind = node.toFind
    generateRuleSets("/q e tofind", nToFind)

    // OnFind
    def nOnFind = node.onFind
    generateRuleSets("/q e onfind", nOnFind)

    // ToAdvance
    def nToAdvance = node.toAdvance
    generateRuleSets("/q e toadvance", nToAdvance)

    // OnAdvance
    def nOnAdvance = node.onAdvance
    generateRuleSets("/q e onadvance", nOnAdvance)

    // OnComplete
    def nOnComplete = node.onComplete
    generateRuleSets("/q e oncomplete", nOnComplete)

    println()
}

static void generateRuleSets(String base, def rules) {
    for (def ruleLine: rules) {
        println(base + " " + stringifyRuleLine(ruleLine))
    }
}

static String stringifyRuleLine(def json) {
    String ret = ''
    if (json.wanted == false) ret += '!'
    ret += json.ruleId
    ret += ' '
    if (json.data != null) ret += json.data
    return ret
}
