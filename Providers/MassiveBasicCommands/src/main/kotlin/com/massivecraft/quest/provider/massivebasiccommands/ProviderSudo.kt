package com.massivecraft.quest.provider.massivebasiccommands

import com.massivecraft.quest.command.CommandProvider
import com.massivecraft.quest.command.CommandProviderList
import com.massivecraft.quest.command.CommandProviderReceiver

class ProviderSudo: CommandProvider {

    var target: String = "{p}"

    var innerCommands: CommandProviderList = CommandProviderList()

    fun innerCommands(config: CommandProviderList.() -> Unit) {
        innerCommands.apply(config)
    }

    override fun toCommandStrings(): List<String> {
        val cmdBase = "/sudo $target"
        return innerCommands.getCommands().map { "$cmdBase $it" }
    }
}

fun CommandProviderReceiver.sudo(config: ProviderSudo.() -> Unit): ProviderSudo {
    val ret = ProviderSudo().apply(config)
    cmd(ret)
    return ret
}

private fun exampleSudo() {
    val providerList = CommandProviderList()
    providerList.apply {
        sudo {
            target = "{p}"
            innerCommands {
                cmd("/t set fallimmune")
            }
        }
    }
}
