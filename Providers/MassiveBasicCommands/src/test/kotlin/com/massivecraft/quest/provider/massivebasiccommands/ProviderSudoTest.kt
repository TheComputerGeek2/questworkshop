package com.massivecraft.quest.provider.massivebasiccommands

import kotlin.test.Test

class ProviderSudoTest {
    @Test
    fun testSudo() {
        val provider = ProviderSudo()
        provider.target = "{p}"
        provider.innerCommands {
            cmd("/t set fallimmune")
        }
        val expected = arrayOf("/sudo {p} /t set fallimmune").toList()
        val output = provider.toCommandStrings().toList()
        assert(expected == output)
    }
}
