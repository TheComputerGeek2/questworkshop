
plugins {
    `java-gradle-plugin`
}

apply(plugin = "kotlin")

gradlePlugin {
    plugins {
        create("questgradle") {
            id = "com.massivecraft.quest.questgradle"
            implementationClass = "com.massivecraft.quest.gradle.QuestGradle"
        }
    }
}


dependencies {
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.21")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.3.21")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:1.3.21")
    testImplementation("junit:junit:4.12")
    testImplementation(gradleTestKit())
}
