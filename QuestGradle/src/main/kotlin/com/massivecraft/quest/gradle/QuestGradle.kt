package com.massivecraft.quest.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import java.net.URI

class QuestGradle: Plugin<Project> {
    override fun apply(target: Project) {
        target.pluginManager.apply("java")
        target.pluginManager.apply("org.jetbrains.kotlin.jvm")
        target.repositories.run {
            maven { repo ->
                repo.name = "QuestWorkshopRepo"
                repo.url = URI("https://jitpack.io")
                repo.content { content ->
                    content.includeGroup("org.bitbucket.TheComputerGeek2.QuestWorkshop")
                    //language=RegExp
                    content.includeGroupByRegex("org.bitbucket\\.TheComputerGeek2.*")
                }
            }

            mavenCentral()

            maven { repo ->
                repo.url = URI("https://oss.sonatype.org/content/repositories/snapshots")
            }

            maven { repo ->
                repo.url = URI("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
            }
        }
        target.afterEvaluate {
            it.dependencies.add("implementation", "org.jetbrains.kotlin:kotlin-stdlib")
            it.dependencies.add("implementation", "org.jetbrains.kotlin:kotlin-script-runtime")
        }
    }
}
