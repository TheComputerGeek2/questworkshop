package com.massivecraft.quest.gradle

import org.gradle.api.artifacts.dsl.DependencyHandler

object QuestGradleUtil {
    /**
     * Builds the dependency notation for the named Kotlin [module] at the given [version].
     *
     * @param module simple name of the Kotlin module, for example "reflect".
     * @param version optional desired version, unspecified if null.
     */
    fun DependencyHandler?.questWorkshop(module: String, version: String? = null): String {
        return "org.bitbucket.TheComputerGeek2.QuestWorkshop:$module${version?.let { ":$version" } ?: ""}"
    }

}
