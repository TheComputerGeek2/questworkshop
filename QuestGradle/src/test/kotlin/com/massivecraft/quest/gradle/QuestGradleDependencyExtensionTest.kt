package com.massivecraft.quest.gradle

import org.gradle.testkit.runner.GradleRunner
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File

class QuestGradleDependencyExtensionTest {

    @JvmField
    @Rule public val testProjectDir = TemporaryFolder()
    lateinit var buildFile: File

    @Before
    fun setup() {
        buildFile = testProjectDir.newFile("build.gradle.kts")
    }

    @Test
    fun testDependencyExtension() {
        val buildFileContent = """
            import com.massivecraft.quest.gradle.QuestGradleUtil.questWorkshop

            buildscript {
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.21")
    }
}

plugins {
  id("com.massivecraft.quest.questgradle")
}

dependencies {
    implementation(questWorkshop("QuestWriter", "1662a45541"))
}
        """.trimIndent()

        buildFile.writeText(buildFileContent)

        val result = GradleRunner.create()
                .withPluginClasspath()
                .withProjectDir(testProjectDir.root)
                .withArguments("tasks", "--all")
                .build()

        println(result.output)

        result.tasks.forEach {
            println("Task: $it")
            println("path: ${it.path}")
        }
    }
}
