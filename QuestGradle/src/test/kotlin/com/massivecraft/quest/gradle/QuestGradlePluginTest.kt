package com.massivecraft.quest.gradle


import com.massivecraft.quest.gradle.QuestGradleUtil.questWorkshop
import org.gradle.testfixtures.ProjectBuilder
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertNotNull

class QuestGradlePluginTest {

    @Test
    fun addsJavaKotlinPlugins() {
        val project = ProjectBuilder.builder().build()
        project.pluginManager.apply("com.massivecraft.quest.questgradle")
        assertNotNull(project.plugins.getPlugin("java"))
        assertNotNull(project.plugins.getPlugin("kotlin"))
    }

    @Test
    fun `dependencies added`() {
        val project = ProjectBuilder.builder().build()
        project.pluginManager.apply("com.massivecraft.quest.questgradle")
        project.dependencies.run {
            add("implementation", questWorkshop("QuestWriter", "1662a45541"))
        }
        project.configurations.run {
            create("inspecthelper").extendsFrom(getByName("implementation"))
        }
        val path = project.configurations.getByName("inspecthelper").asPath
        assertNotNull(path)
        assertFalse(path.isBlank(), "path should not be blank")
        println(path.replace(":", "\n"))
    }

}
