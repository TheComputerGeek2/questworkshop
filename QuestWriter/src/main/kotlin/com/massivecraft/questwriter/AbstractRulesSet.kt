package com.massivecraft.questwriter

abstract class AbstractRulesSet<R: Rule<*>>: ArrayList<R>()
{
    var owningNode: Node? = null

    val node: Node
        get() {
            return owningNode!!
        }

}
