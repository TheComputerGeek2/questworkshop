package com.massivecraft.questwriter

/**
 * A node to specify part of a quest.
 */
open class Node constructor(config: (Node.()-> Unit)? = null)
{
    /**
     * This must be assigned on every node
     * TODO hunt down the conventions for node ids
     */
    var id: String? = null

    /**
     * The name to use for the node when displaying to players.
     */
    var name: String? = null

    /**
     * A description for the node, it accepts MassiveCore formatting elements
     */
    var description: String? = null

    /**
     * The number of times this node must be advanced in order to be completed.
     * Null results in the plugins's default of 1 being used.
     * @throws [IllegalArgumentException] if value is less than zero
     */
    var times: Int? = null
        set(value)
        {
            if (value != null && value < 0) throw IllegalArgumentException("$value")
            field = value
        }

    var category: Boolean? = null

    /**
     * Should this node be visible to players? Default handling is to be visible.
     */
    var visible: Boolean? = null

    var cooldown: String? = null

    /**
     * The set of rules which define the criteria for this node to be found.
     */
    var rulesToFind: ConditionRules = ConditionRules().apply { owningNode = this@Node }

    /**
     * The set of rules which define the criteria for this node to advance.
     */
    var rulesToAdvance: ConditionRules = ConditionRules().apply { owningNode = this@Node }

    var rulesOnFind: ActionRules = ActionRules().apply { owningNode = this@Node }

    var rulesOnAdvance: ActionRules = ActionRules().apply { owningNode = this@Node }

    var rulesOnComplete: ActionRules = ActionRules().apply { owningNode = this@Node }

    var locked: Boolean? = null

    init
    {
        if (config != null)
        {
            config()
        }
    }

}
