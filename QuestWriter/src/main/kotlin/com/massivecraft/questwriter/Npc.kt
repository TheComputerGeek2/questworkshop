package com.massivecraft.questwriter

/**
 * A non player character to be referenced from a quest.
 */
data class Npc(
        val name: String,
        var description: String? = null
) {
    constructor(): this("UNNAMED")
}
