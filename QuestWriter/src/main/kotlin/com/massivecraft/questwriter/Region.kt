package com.massivecraft.questwriter

data class Region constructor(var world: String, var regionName: String)
{
    constructor(): this("UNCONFIGURED", "UNCONFIGURED")
}
