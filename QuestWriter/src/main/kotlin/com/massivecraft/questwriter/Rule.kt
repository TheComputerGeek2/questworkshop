package com.massivecraft.questwriter

interface Rule<D>
{
    var wanted: Boolean
    fun ruleLine(): String
    var data: D
    val dataAcceptor: RuleData<D>
}

interface ConditionRule<D>: Rule<D>

interface TriggerRule<D>: ConditionRule<D>

interface ActionRule<D>: Rule<D>
