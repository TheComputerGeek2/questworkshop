package com.massivecraft.questwriter

abstract class RuleData<D> {
    // Throw an exception if not acceptable
    abstract fun acceptData(data: D): D

    abstract fun dataToCommandUsable(data: D): String
}
