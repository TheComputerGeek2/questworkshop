package com.massivecraft.questwriter

data class SoundEffect(
        var soundId: String,
        var volume: Double = 1.0,
        var pitch: Double = 1.0
)
