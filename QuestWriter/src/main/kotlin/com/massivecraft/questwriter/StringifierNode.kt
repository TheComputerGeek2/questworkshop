package com.massivecraft.questwriter

object StringifierNode
{
    fun stringify(node: Node): List<String>
    {
        val ret = ArrayList<String>()

        // id
        if (node.id == null) throw IllegalStateException("all quest nodes must have an id")
        ret.add("/q new ${node.id}")

        // name
        if (node.name != null)
        {
            ret.add("/q e name ${node.name}")
        }

        // description
        if (node.description != null)
        {
            ret.add("/q e desc ${node.description}")
        }

        // times
        if (node.times != null)
        {
            ret.add("/q e times ${node.times}")
        }

        // category
        if (node.category != null)
        {
            ret.add("/q e category ${node.category}")
        }

        // visible
        if (node.visible != null)
        {
            ret.add("/q e visible ${node.visible}")
        }

        // cooldown
        // TODO see about making this a bit friendlier with larger time units
        if (node.cooldown != null)
        {
            ret.add("/q e cooldown ${node.cooldown}")
        }

        // tofind
        if (!node.rulesToFind.isEmpty())
        {
            ret.addAll(node.rulesToFind.map { "/q e tofind ${it.ruleLine()}" })
        }

        // toadvance
        if (!node.rulesToAdvance.isEmpty())
        {
            ret.addAll(node.rulesToAdvance.map { "/q e toadvance ${it.ruleLine()}" })
        }

        // onfind
        if (!node.rulesOnFind.isEmpty())
        {
            ret.addAll(node.rulesOnFind.map { "/q e onfind ${it.ruleLine()}" })
        }

        // onadvance
        if (!node.rulesOnAdvance.isEmpty())
        {
            ret.addAll(node.rulesOnAdvance.map { "/q e onadvance ${it.ruleLine()}" })
        }

        // oncomplete
        if (!node.rulesOnComplete.isEmpty())
        {
            ret.addAll(node.rulesOnComplete.map { "/q e oncomplete ${it.ruleLine()}" })
        }

        // locked
        if (node.locked != null)
        {
            ret.add("/q e locked ${node.locked}")
        }

        return ret
    }

}
