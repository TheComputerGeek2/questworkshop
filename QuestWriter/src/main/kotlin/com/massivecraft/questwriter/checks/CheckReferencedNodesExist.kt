package com.massivecraft.questwriter.checks

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.data.DataNodeId
import com.massivecraft.questwriter.quest.rules
import java.util.HashSet

fun <N: Node, C: Collection<N>> C.getNodesReferenced(): Set<String> {
    val referencedNodeIds = HashSet<String>()
    this.flatMap { it.rules() }
            .filter { it.dataAcceptor === DataNodeId }
            .forEach { referencedNodeIds.add(it.data as String) }
    return referencedNodeIds
}

fun <C: Collection<Node>> C.getDeclaredNodes(): Set<String> = this.map { it.id!! }.toHashSet()

fun <C: Collection<Node>> C.getExternalNodeIds() = getNodesReferenced() - getDeclaredNodes()
