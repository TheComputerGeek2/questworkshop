package com.massivecraft.questwriter.container

data class PotionEffect(
        var type: String = "ABSORPTION",
        var amplifier: Int = 0,
        var duration: Int = 0,
        var ambient: Boolean = false,
        var particles: Boolean = true
)
{
    val commandString: String
        get() = "$type $amplifier $duration $ambient $particles"
}
