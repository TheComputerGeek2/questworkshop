package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData
import org.bukkit.entity.EntityType
import kotlin.IllegalArgumentException

object DataEntityType: RuleData<EntityType>()
{
    override fun acceptData(data: EntityType): EntityType = data

    override fun dataToCommandUsable(data: EntityType): String = data.name

}

object DataLivingEntityType: RuleData<EntityType>()
{
    override fun acceptData(data: EntityType): EntityType
    {
        if (!data.isAlive) throw IllegalArgumentException("${data.name} is not living!")
        return data
    }

    override fun dataToCommandUsable(data: EntityType): String = data.name

}
