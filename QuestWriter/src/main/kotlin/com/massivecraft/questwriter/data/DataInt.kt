package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData

sealed class DataInt: RuleData<Int>()
{
    override fun dataToCommandUsable(data: Int): String = "$data"

    companion object
    {
        object Any: DataInt()
        {
            override fun acceptData(data: Int): Int = data
        }

        object AboveZero: DataInt()
        {
            override fun acceptData(data: Int): Int
            {
                if (data <= 0) throw IllegalArgumentException("$data is not above zero")
                return data
            }
        }
    }

}
