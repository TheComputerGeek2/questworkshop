package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData

data class MaterialDataCount(
        var material: String, // can be a material name, id, or *
        var data: String? = null,
        var count: String? = null
)

object DataMaterialDataCount: RuleData<MaterialDataCount>()
{
    override fun acceptData(data: MaterialDataCount): MaterialDataCount = data

    override fun dataToCommandUsable(data: MaterialDataCount): String
    {
        var ret = data.material
        if (data.data != null) ret += ":${data.data}"
        if (data.count != null) ret += "x${data.count}"
        return ret
    }
}

data class MaterialData(var material: String, var data: String? = null)

object DataMaterialData: RuleData<MaterialData>()
{
    override fun acceptData(data: MaterialData): MaterialData = data

    override fun dataToCommandUsable(data: MaterialData): String
    {
        var ret = data.material
        if (data.data != null) ret += ":${data.data}"
        return ret
    }
}
