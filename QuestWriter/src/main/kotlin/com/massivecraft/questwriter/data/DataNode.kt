package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.RuleData

object DataNode: RuleData<Node>()
{
    override fun acceptData(data: Node): Node = data

    override fun dataToCommandUsable(data: Node): String = data.id!!

}
