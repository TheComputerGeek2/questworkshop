package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData

object DataNodeId: RuleData<String>()
{
    override fun acceptData(data: String): String = data

    override fun dataToCommandUsable(data: String): String = data.trim()
}
