package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData

object DataNone: RuleData<Unit>()
{
    override fun acceptData(data: Unit): Unit = data

    override fun dataToCommandUsable(data: Unit): String = ""

}
