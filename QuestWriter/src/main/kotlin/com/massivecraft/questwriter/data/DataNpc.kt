package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.Npc
import com.massivecraft.questwriter.RuleData

object DataNpc: RuleData<Npc>()
{
    override fun acceptData(data: Npc): Npc = data

    // TODO strip out colors and lowercase it
    override fun dataToCommandUsable(data: Npc): String = data.name

}
