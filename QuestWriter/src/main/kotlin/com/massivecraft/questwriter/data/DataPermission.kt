package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.Permission
import com.massivecraft.questwriter.RuleData

object DataPermission: RuleData<Permission>()
{
    override fun acceptData(data: Permission): Permission = data

    override fun dataToCommandUsable(data: Permission): String = data.nodeId

}
