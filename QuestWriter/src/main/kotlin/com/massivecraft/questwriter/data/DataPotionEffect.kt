package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData
import com.massivecraft.questwriter.container.PotionEffect

object DataPotionEffect: RuleData<PotionEffect>()
{
    override fun acceptData(data: PotionEffect): PotionEffect = data

    override fun dataToCommandUsable(data: PotionEffect): String = data.commandString

}
