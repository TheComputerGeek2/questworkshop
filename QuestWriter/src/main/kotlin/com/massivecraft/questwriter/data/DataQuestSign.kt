package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.QuestSign
import com.massivecraft.questwriter.RuleData

object DataQuestSign: RuleData<QuestSign>()
{
    override fun acceptData(data: QuestSign): QuestSign = data

    override fun dataToCommandUsable(data: QuestSign): String = data.text

}
