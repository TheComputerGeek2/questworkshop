package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.Region
import com.massivecraft.questwriter.RuleData

object DataRegion: RuleData<Region>()
{
    override fun acceptData(data: Region): Region = data

    override fun dataToCommandUsable(data: Region): String = "${data.world}.${data.regionName}"

}
