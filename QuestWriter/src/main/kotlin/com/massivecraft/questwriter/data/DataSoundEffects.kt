package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData
import com.massivecraft.questwriter.SoundEffect

object DataSoundEffects: RuleData<MutableList<SoundEffect>>()
{
    override fun acceptData(data: MutableList<SoundEffect>): MutableList<SoundEffect> = data

    override fun dataToCommandUsable(data: MutableList<SoundEffect>): String
    {
        var ret = ""
        data.map { "${it.soundId} ${it.volume} ${it.pitch}" }.forEach { ret += "$it " }
        return ret.trim()
    }

}
