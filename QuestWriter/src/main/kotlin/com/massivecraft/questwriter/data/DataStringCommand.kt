package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData
import kotlin.IllegalArgumentException

object DataStringCommand: RuleData<String>()
{
    override fun acceptData(data: String): String
    {
        if (data.isBlank()) throw IllegalArgumentException("data must not be blank")
        if (!data.startsWith("/")) throw IllegalArgumentException("data must start with the leading slash")
        return data
    }

    override fun dataToCommandUsable(data: String): String = data.trim()

}
