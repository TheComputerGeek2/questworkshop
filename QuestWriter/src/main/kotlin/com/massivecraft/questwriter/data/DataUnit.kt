package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData

object DataUnit: RuleData<Unit>() {
    override fun dataToCommandUsable(data: Unit): String = ""

    override fun acceptData(data: Unit): Unit = data

}
