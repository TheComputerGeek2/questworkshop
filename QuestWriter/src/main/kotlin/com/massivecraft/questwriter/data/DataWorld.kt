package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.RuleData
import com.massivecraft.questwriter.World

object DataWorld: RuleData<World>()
{
    override fun acceptData(data: World): World = data

    override fun dataToCommandUsable(data: World): String = data.name.toLowerCase()
}
