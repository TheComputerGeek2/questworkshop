package com.massivecraft.questwriter.dsl

import com.massivecraft.questwriter.ActionRules
import com.massivecraft.questwriter.ConditionRules
import com.massivecraft.questwriter.Node

inline fun Node.toFind(block: ConditionRules.() -> Unit): ConditionRules = rulesToFind.apply(block)
inline fun Node.toAdvance(block: ConditionRules.() -> Unit): ConditionRules = rulesToAdvance.apply(block)
inline fun Node.onFind(block: ActionRules.() -> Unit): ActionRules = rulesOnFind.apply(block)
inline fun Node.onAdvance(block: ActionRules.() -> Unit): ActionRules = rulesOnAdvance.apply(block)
inline fun Node.onComplete(block: ActionRules.() -> Unit): ActionRules = rulesOnComplete.apply(block)
