package com.massivecraft.questwriter.dsl

import com.massivecraft.questwriter.ActionRules
import com.massivecraft.questwriter.ConditionRule
import com.massivecraft.questwriter.ConditionRules
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnFirstJoin

// Just in case something needs to change
typealias ActionRuleHolder = ActionRules
typealias ConditionRuleHolder = ConditionRules
typealias TriggerRuleHolder = ConditionRules

fun <R: ConditionRule<*>> R.negate(): R {
    wanted = !wanted
    return this
}

fun TriggerRuleHolder.onFirstJoin()
{
    add(RuleOnFirstJoin())
}
