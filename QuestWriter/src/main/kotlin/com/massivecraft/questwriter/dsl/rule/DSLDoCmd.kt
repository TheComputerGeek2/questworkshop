package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.quest.command.CommandProviderList
import com.massivecraft.questwriter.ActionRules
import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.dsl.ActionRuleHolder
import com.massivecraft.questwriter.rules.action.RuleDoCmd

/**
 * Add an instruction to unfind the tree with [root] as the root node
 */
fun <S: ActionRules> S.unfindTree(vararg root: Node)
{
    root.forEach {
        this@unfindTree.add(RuleDoCmd("/q p ut {p} ${it.id}"))
    }
}

/**
 * Add an instruction to unfind the tree with [root] as the root node
 */
fun <S: ActionRules> S.unfindTree(vararg root: String)
{
    root.forEach {
        this@unfindTree.add(RuleDoCmd("/q p ut {p} $it"))
    }
}

fun <S: ActionRules> S.doCmds(config: CommandProviderList.() -> Unit) {
    val providerList = CommandProviderList()
    providerList.config()
    providerList.forEach { provider ->
        provider.toCommandStrings().forEach { cmdString ->
            this@doCmds.add(RuleDoCmd(cmdString))
        }
    }
}

fun <S: ActionRules> S.doCmds(vararg cmds: String)
{
    cmds.forEach {
        this@doCmds.add(RuleDoCmd(it))
    }
}

fun ActionRuleHolder.doCmd(config: RuleDoCmd.() -> Unit)
{
    val rule = RuleDoCmd("/broadcast someone didn't pay attention when writing a quest")
    rule.config()
    add(rule)
}
