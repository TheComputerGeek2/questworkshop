package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.Rule
import com.massivecraft.questwriter.data.MaterialData
import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfInBlock
import com.massivecraft.questwriter.rules.condition.RuleIfNearBlock
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnBreakBlock
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnClickBlock
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnClickFrame
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnConsume
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnEnterBlock
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnExitBlock
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnFishFish
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnPlaceBlock
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnPlacedBlock

fun <R: Rule<MaterialData>> R.materialData(config: MaterialData.() -> Unit): R
{
    data.config()
    return this
}

fun TriggerRuleHolder.onClickBlock(config: RuleOnClickBlock.() -> Unit)
{
    val rule = RuleOnClickBlock(MaterialData("STONE"))
    rule.config()
    add(rule)
}

fun TriggerRuleHolder.onClickBlock(config: MaterialData)
{
    add(RuleOnClickBlock(config))
}

fun TriggerRuleHolder.onBreakBlock(data: MaterialData)
{
    add(RuleOnBreakBlock(data))
}

fun TriggerRuleHolder.onClickFrame(data: MaterialData)
{
    add(RuleOnClickFrame(data))
}

fun TriggerRuleHolder.onConsume(data: MaterialData)
{
    add(RuleOnConsume(data))
}

fun TriggerRuleHolder.onEnterBlock(data: MaterialData)
{
    add(RuleOnEnterBlock(data))
}

fun TriggerRuleHolder.onExitBlock(data: MaterialData)
{
    add(RuleOnExitBlock(data))
}

fun TriggerRuleHolder.onPlaceBlock(data: MaterialData)
{
    add(RuleOnPlaceBlock(data))
}

fun TriggerRuleHolder.onPlacedBlock(data: MaterialData)
{
    add(RuleOnPlacedBlock(data))
}

fun TriggerRuleHolder.onFishFish(data: MaterialData)
{
    add(RuleOnFishFish(data))
}

fun ConditionRuleHolder.ifInBlock(data: MaterialData)
{
    add(RuleIfInBlock(data))
}

fun ConditionRuleHolder.ifNotInBlock(vararg data: MaterialData)
{
    data.forEach { add(RuleIfInBlock(it).negate()) }
}

fun ConditionRuleHolder.ifNearBlock(vararg data: MaterialData)
{
    data.forEach { add(RuleIfInBlock(it)) }
}

fun ConditionRuleHolder.ifNotNearBlock(vararg data: MaterialData)
{
    data.forEach { add(RuleIfNearBlock(it).negate()) }
}
