package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.data.MaterialDataCount
import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnCraft

fun TriggerRuleHolder.onCraft(data: MaterialDataCount)
{
    add(RuleOnCraft(data))
}
