package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnChat

fun TriggerRuleHolder.onChat(msg: String)
{
    add(RuleOnChat(msg))
}
