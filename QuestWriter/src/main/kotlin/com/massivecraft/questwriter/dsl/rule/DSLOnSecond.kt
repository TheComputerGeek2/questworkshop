package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnSecond

fun TriggerRuleHolder.onSecond(interval: Int)
{
    add(RuleOnSecond(interval))
}
