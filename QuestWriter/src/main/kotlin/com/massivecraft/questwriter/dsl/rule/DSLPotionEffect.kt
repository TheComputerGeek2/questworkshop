package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.Rule
import com.massivecraft.questwriter.container.PotionEffect
import com.massivecraft.questwriter.dsl.ActionRuleHolder
import com.massivecraft.questwriter.rules.action.RuleDoPotionEffect

// PotionEffect rules
fun <R: Rule<PotionEffect>> R.effect(config: PotionEffect.() -> Unit): R
{
    data.config()
    return this
}

fun ActionRuleHolder.doPotionEffect(config: RuleDoPotionEffect.() -> Unit)
{
    add(RuleDoPotionEffect().apply(config))
}
