package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfPremium

fun ConditionRuleHolder.ifPremium()
{
    add(RuleIfPremium())
}

fun ConditionRuleHolder.ifNotPremium()
{
    add(RuleIfPremium().negate())
}
