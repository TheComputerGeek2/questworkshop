package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.rules.condition.RuleIfSlotsEmpty

// TODO replace this with a non function literal
fun ConditionRuleHolder.ifSlotsEmpty(config: RuleIfSlotsEmpty.() -> Unit)
{
    add(RuleIfSlotsEmpty(1).apply(config))
}

fun ConditionRuleHolder.ifSlotsEmpty(slots: Int)
{
    add(RuleIfSlotsEmpty(slots))
}
