package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.SoundEffect
import com.massivecraft.questwriter.dsl.ActionRuleHolder
import com.massivecraft.questwriter.rules.action.RuleDoHearSound

fun MutableList<SoundEffect>.sound(config: SoundEffect.() -> Unit)
{
    add(SoundEffect("NONEXISTENT-SOUND").apply(config))
}

fun ActionRuleHolder.doHearSound(config: MutableList<SoundEffect>.() -> Unit)
{
    val rule = RuleDoHearSound()
    rule.data.config()
    add(rule)
}
