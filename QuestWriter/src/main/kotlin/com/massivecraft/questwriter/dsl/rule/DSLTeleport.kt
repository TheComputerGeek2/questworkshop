package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.ActionRuleHolder
import com.massivecraft.questwriter.rules.action.RuleDoTeleport

fun ActionRuleHolder.doTeleport(destination: String)
{
    add(RuleDoTeleport(destination))
}
