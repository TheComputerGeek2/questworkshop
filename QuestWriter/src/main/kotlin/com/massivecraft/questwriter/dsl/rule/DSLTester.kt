package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfTester

fun ConditionRuleHolder.ifTester()
{
    add(RuleIfTester())
}

fun ConditionRuleHolder.ifNotTester()
{
    add(RuleIfTester().negate())
}
