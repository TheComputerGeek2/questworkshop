package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfTimeDay
import com.massivecraft.questwriter.rules.condition.RuleIfTimeNight

fun ConditionRuleHolder.ifDay()
{
    add(RuleIfTimeDay())
}

fun ConditionRuleHolder.ifNotDay()
{
    add(RuleIfTimeDay().negate())
}

fun ConditionRuleHolder.ifNight()
{
    add(RuleIfTimeNight())
}

fun ConditionRuleHolder.ifNotNight()
{
    add(RuleIfTimeNight().negate())
}
