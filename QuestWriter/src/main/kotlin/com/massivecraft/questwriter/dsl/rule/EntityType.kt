package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfNearMob
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnGankMob
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnImpactMob
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnKillMob
import org.bukkit.entity.EntityType

fun ConditionRuleHolder.ifNearMob(vararg types: EntityType)
{
    types.forEach { add(RuleIfNearMob(it)) }
}

fun ConditionRuleHolder.ifNotNearMob(vararg types: EntityType)
{
    types.forEach { add(RuleIfNearMob(it).negate()) }
}

fun TriggerRuleHolder.onGankMob(type: EntityType)
{
    add(RuleOnGankMob(type))
}

fun TriggerRuleHolder.onImpactMob(type: EntityType)
{
    add(RuleOnImpactMob(type))
}

fun TriggerRuleHolder.onKillMob(type: EntityType)
{
    add(RuleOnKillMob(type))
}
