package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.data.MaterialDataCount
import com.massivecraft.questwriter.dsl.ActionRuleHolder
import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.action.RuleDoGiveItem
import com.massivecraft.questwriter.rules.action.RuleDoLoseItem
import com.massivecraft.questwriter.rules.condition.RuleIfHaveItem

fun ActionRuleHolder.doGiveItem(config: RuleDoGiveItem.() -> Unit)
{
    add(RuleDoGiveItem("").apply(config))
}

fun ActionRuleHolder.doGiveItem(spawnCmd: String)
{
    add(RuleDoGiveItem(spawnCmd))
}

fun ConditionRuleHolder.ifHaveItem(spec: MaterialDataCount)
{
    add(RuleIfHaveItem(spec))
}

inline fun ConditionRuleHolder.ifHaveItem(config: MaterialDataCount.()-> Unit)
{
    add(RuleIfHaveItem(MaterialDataCount("STONE").apply(config)))
}

fun ConditionRuleHolder.ifDoesntHaveItem(spec: MaterialDataCount)
{
    add(RuleIfHaveItem(spec).negate())
}

fun ActionRuleHolder.doLoseItem(spec: MaterialDataCount)
{
    add(RuleDoLoseItem(spec))
}

inline fun ActionRuleHolder.doLoseItem(config: MaterialDataCount.()-> Unit)
{
    add(RuleDoLoseItem(MaterialDataCount("STONE").apply(config)))
}
