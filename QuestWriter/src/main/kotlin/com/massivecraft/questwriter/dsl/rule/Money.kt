package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.ActionRuleHolder
import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.action.RuleDoGiveMoney
import com.massivecraft.questwriter.rules.action.RuleDoLoseMoney
import com.massivecraft.questwriter.rules.condition.RuleIfHaveMoney

fun ActionRuleHolder.doGiveMoney(amount: Int)
{
    add(RuleDoGiveMoney(amount))
}

fun ActionRuleHolder.doLoseMoney(amount: Int)
{
    add(RuleDoLoseMoney(amount))
}

fun ConditionRuleHolder.ifHaveMoney(amount: Int)
{
    add(RuleIfHaveMoney(amount))
}

fun ConditionRuleHolder.ifDoesntHaveMoney(amount: Int)
{
    add(RuleIfHaveMoney(amount).negate())
}
