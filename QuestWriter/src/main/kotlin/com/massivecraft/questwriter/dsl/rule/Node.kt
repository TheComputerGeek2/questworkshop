package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfCompleted
import com.massivecraft.questwriter.rules.condition.RuleIfFound
import com.massivecraft.questwriter.rules.condition.RuleIfToAdvance

fun ConditionRuleHolder.ifCompleted(vararg nodes: Node)
{
    nodes.forEach { add(RuleIfCompleted(it)) }
}

fun ConditionRuleHolder.ifCompleted(vararg nodes: String)
{
    nodes.forEach { add(RuleIfCompleted(it)) }
}

fun ConditionRuleHolder.ifIncomplete(vararg nodes: Node)
{
    nodes.forEach { add(RuleIfCompleted(it).negate()) }
}

fun ConditionRuleHolder.ifIncomplete(vararg nodes: String)
{
    nodes.forEach { add(RuleIfCompleted(it).negate()) }
}

fun ConditionRuleHolder.ifFound(vararg nodes: Node)
{
    nodes.forEach { add(RuleIfFound(it)) }
}

fun ConditionRuleHolder.ifFound(vararg nodes: String)
{
    nodes.forEach { add(RuleIfFound(it)) }
}

fun ConditionRuleHolder.ifNotFound(vararg nodes: Node)
{
    nodes.forEach { add(RuleIfFound(it).negate()) }
}

fun ConditionRuleHolder.ifNotFound(vararg nodes: String)
{
    nodes.forEach { add(RuleIfFound(it).negate()) }
}

fun ConditionRuleHolder.ifToAdvance(vararg nodes: Node)
{
    nodes.forEach { add(RuleIfToAdvance(it)) }
}

fun ConditionRuleHolder.ifToAdvance(vararg nodes: String)
{
    nodes.forEach { add(RuleIfToAdvance(it)) }
}

fun ConditionRuleHolder.ifInProgress(vararg nodes: Node)
{
    nodes.forEach {
        ifFound(it)
        ifIncomplete(it)
    }
}

fun ConditionRuleHolder.ifInProgress(vararg nodes: String)
{
    nodes.forEach {
        ifFound(it)
        ifIncomplete(it)
    }
}
