package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.dsl.ActionRuleHolder
import com.massivecraft.questwriter.rules.action.RuleDoAdvance
import com.massivecraft.questwriter.rules.action.RuleDoAdvanceRandomChild
import com.massivecraft.questwriter.rules.action.RuleDoComplete
import com.massivecraft.questwriter.rules.action.RuleDoCompleteRandomChild
import com.massivecraft.questwriter.rules.action.RuleDoFind
import com.massivecraft.questwriter.rules.action.RuleDoFindRandomChild
import com.massivecraft.questwriter.rules.action.RuleDoUnfind
import com.massivecraft.questwriter.rules.action.RuleDoUnfindRandomChild

fun ActionRuleHolder.doFind(vararg nodes: Node)
{
    nodes.forEach { add(RuleDoFind(it)) }
}

fun ActionRuleHolder.doFind(vararg nodes: String)
{
    nodes.forEach { add(RuleDoFind(it)) }
}

fun ActionRuleHolder.doAdvance(vararg nodes: Node)
{
    nodes.forEach { add(RuleDoAdvance(it)) }
}

fun ActionRuleHolder.doAdvance(vararg nodes: String)
{
    nodes.forEach { add(RuleDoAdvance(it)) }
}

fun ActionRuleHolder.doFindRandomChild(vararg nodes: Node)
{
    nodes.forEach { add(RuleDoFindRandomChild(it)) }
}

fun ActionRuleHolder.doFindRandomChild(vararg nodes: String)
{
    nodes.forEach { add(RuleDoFindRandomChild(it)) }
}

fun ActionRuleHolder.doAdvanceRandomChild(vararg nodes: Node)
{
    nodes.forEach { add(RuleDoAdvanceRandomChild(it)) }
}

fun ActionRuleHolder.doAdvanceRandomChild(vararg nodes: String)
{
    nodes.forEach { add(RuleDoAdvanceRandomChild(it)) }
}

fun ActionRuleHolder.doUnfind(vararg nodes: Node)
{
    nodes.forEach { add(RuleDoUnfind(it)) }
}

fun ActionRuleHolder.doUnfind(vararg nodes: String)
{
    nodes.forEach { add(RuleDoUnfind(it)) }
}

fun ActionRuleHolder.doUnfindRandomChild(vararg nodes: Node)
{
    nodes.forEach { add(RuleDoUnfindRandomChild(it)) }
}

fun ActionRuleHolder.doUnfindRandomChild(vararg nodes: String)
{
    nodes.forEach { add(RuleDoUnfindRandomChild(it)) }
}

fun ActionRuleHolder.doComplete(vararg nodes: Node)
{
    nodes.forEach { add(RuleDoComplete(it)) }
}

fun ActionRuleHolder.doComplete(vararg nodes: String)
{
    nodes.forEach { add(RuleDoComplete(it)) }
}

fun ActionRuleHolder.doCompleteRandomChild(vararg nodes: Node)
{
    nodes.forEach { add(RuleDoCompleteRandomChild(it)) }
}

fun ActionRuleHolder.doCompleteRandomChild(vararg nodes: String)
{
    nodes.forEach { add(RuleDoCompleteRandomChild(it)) }
}

// Combined actions
fun ActionRuleHolder.refind(vararg nodes: Node)
{
    nodes.forEach {
        add(RuleDoUnfind(it))
        add(RuleDoFind(it))
    }
}

fun ActionRuleHolder.refind(vararg nodes: String)
{
    nodes.forEach {
        add(RuleDoUnfind(it))
        add(RuleDoFind(it))
    }
}

fun ActionRuleHolder.doFindAndComplete(vararg nodes: Node)
{
    nodes.forEach {
        doFind(it)
        doComplete(it)
    }
}

fun ActionRuleHolder.doFindAndComplete(vararg nodes: String)
{
    nodes.forEach {
        doFind(it)
        doComplete(it)
    }
}
