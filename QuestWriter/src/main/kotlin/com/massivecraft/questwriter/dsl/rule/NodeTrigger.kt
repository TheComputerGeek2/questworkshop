package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnComplete
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnFind

fun TriggerRuleHolder.onFind(node: Node)
{
    add(RuleOnFind(node))
}

fun TriggerRuleHolder.onFind(node: String)
{
    add(RuleOnFind(node))
}

fun TriggerRuleHolder.onComplete(node: Node)
{
    add(RuleOnComplete(node))
}

fun TriggerRuleHolder.onComplete(node: String)
{
    add(RuleOnComplete(node))
}
