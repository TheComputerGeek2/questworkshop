package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.Npc
import com.massivecraft.questwriter.Rule
import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnClickNpc
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnGankNpc
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnImpactNpc
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnKillNpc

// Npc rules
fun <R: Rule<Npc>> R.npc(config: Npc.() -> Unit): R = apply { data.config() }

fun TriggerRuleHolder.onClickNpc(config: RuleOnClickNpc.() -> Unit)
{
    add(RuleOnClickNpc().apply(config))
}

fun TriggerRuleHolder.onClickNpc(npc: Npc)
{
    add(RuleOnClickNpc(npc))
}

fun TriggerRuleHolder.onGankNpc(npc: Npc)
{
    add(RuleOnGankNpc(npc))
}

fun TriggerRuleHolder.onGankNpc(config: RuleOnGankNpc.() -> Unit)
{
    add(RuleOnGankNpc().apply(config))
}

fun TriggerRuleHolder.onImpactNpc(npc: Npc)
{
    add(RuleOnImpactNpc(npc))
}

fun TriggerRuleHolder.onImpactNpc(config: RuleOnImpactNpc.() -> Unit)
{
    add(RuleOnImpactNpc().apply(config))
}

fun TriggerRuleHolder.onKillNpc(npc: Npc)
{
    add(RuleOnKillNpc(npc))
}

fun TriggerRuleHolder.onKillNpc(config: RuleOnKillNpc.() -> Unit)
{
    add(RuleOnKillNpc().apply(config))
}
