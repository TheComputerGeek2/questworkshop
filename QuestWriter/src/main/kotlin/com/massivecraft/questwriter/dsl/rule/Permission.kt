package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.Permission
import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfHavePerm

fun ConditionRuleHolder.ifHavePerm(perm: Permission)
{
    add(RuleIfHavePerm(perm))
}

fun ConditionRuleHolder.ifDoesntHavePerm(perm: Permission)
{
    add(RuleIfHavePerm(perm).negate())
}
