package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.Region
import com.massivecraft.questwriter.Rule
import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfInRegion
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnEnterRegion
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnExitRegion
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnImpactRegion

// Region rules
fun <R: Rule<Region>> R.region(config: Region.() -> Unit): R
{
    data.config()
    return this
}

fun ConditionRuleHolder.ifInRegion(config: RuleIfInRegion.() -> Unit)
{
    add(RuleIfInRegion(Region()).apply(config))
}

fun ConditionRuleHolder.ifNotInRegion(config: RuleIfInRegion.() -> Unit)
{
    add(RuleIfInRegion(Region()).negate().apply(config))
}

fun ConditionRuleHolder.ifInRegion(region: Region)
{
    add(RuleIfInRegion(region))
}

fun ConditionRuleHolder.ifNotInRegion(region: Region)
{
    add(RuleIfInRegion(region).negate())
}

fun TriggerRuleHolder.onEnterRegion(region: Region)
{
    add(RuleOnEnterRegion(region))
}

fun TriggerRuleHolder.onExitRegion(region: Region)
{
    add(RuleOnExitRegion(region))
}

fun TriggerRuleHolder.onImpactRegion(region: Region)
{
    add(RuleOnImpactRegion(region))
}
