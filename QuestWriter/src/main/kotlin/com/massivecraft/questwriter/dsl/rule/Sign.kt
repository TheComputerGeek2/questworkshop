package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.QuestSign
import com.massivecraft.questwriter.Rule
import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnClickSign

fun TriggerRuleHolder.onClickSign(config: RuleOnClickSign.() -> Unit)
{
    add(RuleOnClickSign(QuestSign("")).apply(config))
}

fun TriggerRuleHolder.onClickSign(sign: QuestSign)
{
    add(RuleOnClickSign(sign))
}

fun TriggerRuleHolder.onClickSign(content: String)
{
    add(RuleOnClickSign(QuestSign(content)))
}

// Quest sign rules
fun <R: Rule<QuestSign>> R.sign(config: QuestSign.() -> Unit): R
{
    data.config()
    return this
}
