package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfVampire

fun ConditionRuleHolder.ifVampire()
{
    add(RuleIfVampire())
}

fun ConditionRuleHolder.ifNotVampire()
{
    add(RuleIfVampire().negate())
}
