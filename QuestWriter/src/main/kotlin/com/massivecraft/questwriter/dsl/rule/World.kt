package com.massivecraft.questwriter.dsl.rule

import com.massivecraft.questwriter.World
import com.massivecraft.questwriter.dsl.ConditionRuleHolder
import com.massivecraft.questwriter.dsl.TriggerRuleHolder
import com.massivecraft.questwriter.dsl.negate
import com.massivecraft.questwriter.rules.condition.RuleIfInWorld
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnEnterWorld

fun ConditionRuleHolder.ifInWorld(config: RuleIfInWorld.() -> Unit)
{
    add(RuleIfInWorld(World("regalia")).apply(config))
}

fun ConditionRuleHolder.ifInWorld(world: World)
{
    add(RuleIfInWorld(world))
}

fun ConditionRuleHolder.ifNotInWorld(world: World)
{
    add(RuleIfInWorld(world).negate())
}

fun TriggerRuleHolder.onEnterWorld(world: World)
{
    add(RuleOnEnterWorld(world))
}
