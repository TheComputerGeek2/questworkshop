package com.massivecraft.questwriter.quest

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.Npc
import com.massivecraft.questwriter.Region
import com.massivecraft.questwriter.Rule

// REQUIREMENT LISTINGS
class QuestRequirements
{
    val npcsNeeded: LinkedHashSet<Npc> = LinkedHashSet()
    val regionsNeeded: LinkedHashSet<Region> = LinkedHashSet()
}

// TODO expand to handle items when items are represented better
// TODO make sure that this is always grabbing all information needed to fill [QuestRequirements]
// TODO add signs to the requirement setup
fun <N: Node, C: Collection<N>> C.requirements(): QuestRequirements
{
    val ret = QuestRequirements()

    for (node: N in this)
    {
        // process each requirement
        for (rule: Rule<*> in node.rules())
        {
            val data = rule.data

            when (data)
            {
                is Npc -> ret.npcsNeeded.add(data)
                is Region -> ret.regionsNeeded.add(data)
            }
        }
    }
    return ret
}

fun <N: Node> N.rules(): List<Rule<*>>
{
    val ret = ArrayList<Rule<*>>()
    ret.addAll(rulesToFind)
    ret.addAll(rulesToAdvance)
    ret.addAll(rulesOnFind)
    ret.addAll(rulesOnAdvance)
    ret.addAll(rulesOnComplete)
    return ret
}
