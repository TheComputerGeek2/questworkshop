package com.massivecraft.questwriter.quest

import com.massivecraft.questwriter.Node

/**
 * Generates the command for deleting this quest node
 */
fun <N: Node> N.backout(): String
{
    return "/q delete $id"
}

/**
 * Generate the backout commands for the nodes in this collection
 */
fun <N: Node, C: Collection<N>> C.backout(): List<String>
{
    val ret = ArrayList<String>()
    ret.addAll(this.map { it.backout() })
    return ret
}
