package com.massivecraft.questwriter.quest

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.StringifierNode

fun <N: Node> N.stringify(): List<String>
{
    return StringifierNode.stringify(this)
}

/**
 * Generate the quest implementation commands for the nodes in this collection
 */
fun <N: Node, C: Collection<N>> C.stringify(): List<String>
{
    val ret = ArrayList<String>()
    this.map { it.stringify() }.forEach { ret.addAll(it) }
    return ret
}
