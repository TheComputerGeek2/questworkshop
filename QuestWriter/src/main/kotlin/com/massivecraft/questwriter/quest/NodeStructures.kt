package com.massivecraft.questwriter.quest

import com.massivecraft.questwriter.Node

/**
 * Create a new node which is a child of this node. [innerId] is the id extension on top of this node's id.
 * [config] is an optional field for applying settings to the child node
 */
fun <N: Node> N.child(innerId: String, config: (Node.() -> Unit)? = null): Node
{
    val ret = Node { id = "${this@child.id!!}.$innerId" }
    if (config != null) ret.apply(config)
    return ret
}

fun <N: Node> N.parentId(): String
{
    val lastIndex = id!!.lastIndexOf('.')
    return id!!.substring(0, lastIndex)
}

fun <N: Node> N.siblingIds(vararg innermost: String): Array<String>
{
    val parentId = parentId()
    return innermost.map { "$parentId.$it" }.toTypedArray()
}

fun <N: Node> N.childIds(vararg innermost: String): Array<String>
{
    val baseId = id!!
    return innermost.map { "$baseId.$it" }.toTypedArray()
}
