package com.massivecraft.questwriter.quest

// TODO provide dsl functions for accessing the regions prepared
// TODO make sure instructions make it clear that declaration of regions and npcs should be prior to usage
class Quest
{
    // TODO setup collection of regions
    val regions: QuestRegions = QuestRegions()

    // TODO setup collection of NPCs
    // TODO is it ever needed for an npc to appear in multiple places?
    //  perhaps allow them to have a list of locations and then select from those at use

    // TODO setup collection of signs

    /** The nodes that make up this quest **/
    val questNodes: QuestNodes = QuestNodes()

    fun implementation(): List<String> = questNodes.stringify()

    fun backout(): List<String> = questNodes.backout()

}

inline fun quest(config: Quest.() -> Unit): Quest
{
    val ret = Quest()
    ret.config()
    return ret
}

/**
 * Edit the questNodes of this quest
 */
fun Quest.nodes(config: QuestNodes.() -> Unit)
{
    this.questNodes.config()
}

// TODO create system for checking that all referenced questNodes exist
