package com.massivecraft.questwriter.quest

import com.massivecraft.questwriter.Node

// TODO provide dsl for adding nodes
class QuestNodes: ArrayList<Node>()
{
    inline fun node(config: Node.() -> Unit): Node
    {
        val ret = Node()
        ret.config()
        add(ret)
        return ret
    }
}
