package com.massivecraft.questwriter.rules

import com.massivecraft.questwriter.ActionRule
import com.massivecraft.questwriter.RuleData

abstract class ActionRuleAbstract<D>(
        ruleId: String,
        dataAcceptor: RuleData<D>,
        data: D
): RuleAbstract<D>(ruleId, dataAcceptor, data), ActionRule<D>
