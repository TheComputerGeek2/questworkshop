package com.massivecraft.questwriter.rules

import com.massivecraft.questwriter.ConditionRule
import com.massivecraft.questwriter.RuleData

abstract class ConditionRuleAbstract<D>(
        ruleId: String,
        dataAcceptor: RuleData<D>,
        data: D
): RuleAbstract<D>(ruleId, dataAcceptor, data), ConditionRule<D>
