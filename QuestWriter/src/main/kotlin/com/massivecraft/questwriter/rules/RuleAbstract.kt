package com.massivecraft.questwriter.rules

import com.massivecraft.questwriter.Rule
import com.massivecraft.questwriter.RuleData

/**
 * @param D the data type that is needed for this rule
 */
abstract class RuleAbstract<D> constructor(val ruleId: String, override val dataAcceptor: RuleData<D>, override var data: D): Rule<D>
{
    // TODO do something for setting this field more cleanly
    override var wanted: Boolean = true

    override fun ruleLine(): String
    {
        var ret = "$ruleId ${dataAcceptor.dataToCommandUsable(data)}"
        if (!wanted) ret = "!$ret"
        return ret.trim()
    }

}
