package com.massivecraft.questwriter.rules

import com.massivecraft.questwriter.RuleData
import com.massivecraft.questwriter.TriggerRule

abstract class TriggerRuleAbstract<D>(
        ruleId: String,
        dataAcceptor: RuleData<D>,
        data: D
): ConditionRuleAbstract<D>(ruleId, dataAcceptor, data), TriggerRule<D>
