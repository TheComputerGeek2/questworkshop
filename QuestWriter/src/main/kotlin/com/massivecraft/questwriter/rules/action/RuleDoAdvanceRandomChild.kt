package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.data.DataNodeId
import com.massivecraft.questwriter.rules.ActionRuleAbstract

/**
 * Selects a random child of [com.massivecraft.questwriter.rules.RuleAbstract.data] and advances it.
 */
class RuleDoAdvanceRandomChild constructor(
        node: String
): ActionRuleAbstract<String>(
        "doadvancerandomchild",
        DataNodeId,
        node
) {
    constructor(node: Node): this(node.id!!)
}
