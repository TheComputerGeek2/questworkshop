package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.data.DataStringCommand
import com.massivecraft.questwriter.rules.ActionRuleAbstract

/**
 * Runs a command as the console sender.
 * Uses `{p}` as a placeholder for the player's username.
 *
 * Sample:
 * @sample exampleRuleDoCmd
 *
 * @see [DataStringCommand]
 */
class RuleDoCmd constructor(
        cmd: String
): ActionRuleAbstract<String>(
        "docmd",
        DataStringCommand,
        cmd
) {
    var cmd: String
        get() = data
        set(value) { data = value }
}

// SAMPLES
private fun exampleRuleDoCmd()
{
    RuleDoCmd("/tpo {p} regalia")
}
