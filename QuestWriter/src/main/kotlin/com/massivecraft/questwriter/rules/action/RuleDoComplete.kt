package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.data.DataNodeId
import com.massivecraft.questwriter.rules.ActionRuleAbstract

/**
 * Completes the node specified in [com.massivecraft.questwriter.rules.RuleAbstract.data]
 */
class RuleDoComplete constructor(
        node: String
): ActionRuleAbstract<String>(
        "docomplete",
        DataNodeId,
        node
) {
    constructor(node: Node): this(node.id!!)
}
