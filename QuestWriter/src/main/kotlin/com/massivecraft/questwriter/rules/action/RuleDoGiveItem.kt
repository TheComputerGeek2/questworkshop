package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.data.DataString
import com.massivecraft.questwriter.rules.ActionRuleAbstract

class RuleDoGiveItem constructor(
        data: String
): ActionRuleAbstract<String>(
        "dogiveitem",
        DataString,
        data
) // TODO get structure for this
