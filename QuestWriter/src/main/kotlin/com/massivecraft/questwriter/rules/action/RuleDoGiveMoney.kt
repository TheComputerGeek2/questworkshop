package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.data.DataInt
import com.massivecraft.questwriter.rules.ActionRuleAbstract

/**
 * Gives money to the player. Amount must be above zero.
 */
class RuleDoGiveMoney constructor(
        amount: Int
): ActionRuleAbstract<Int>(
        "dogivemoney",
        DataInt.Companion.AboveZero,
        amount
) {
    var amount: Int
        get() = data
        set(value) { data = value }
}
