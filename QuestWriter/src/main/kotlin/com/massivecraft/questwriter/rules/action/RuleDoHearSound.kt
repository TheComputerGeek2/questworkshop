package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.SoundEffect
import com.massivecraft.questwriter.data.DataSoundEffects
import com.massivecraft.questwriter.rules.ActionRuleAbstract

/**
 * Plays sounds to the player.
 * @see [SoundEffect]
 */
class RuleDoHearSound constructor(
        sounds: MutableList<SoundEffect>
): ActionRuleAbstract<MutableList<SoundEffect>>(
        "dohearsound",
        DataSoundEffects,
        sounds
) {
    constructor(): this(ArrayList())
}
