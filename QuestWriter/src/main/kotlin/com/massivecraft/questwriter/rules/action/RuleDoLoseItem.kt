package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.data.DataMaterialDataCount
import com.massivecraft.questwriter.data.MaterialDataCount
import com.massivecraft.questwriter.rules.ActionRuleAbstract

/**
 * Removes items from the player.
 */
class RuleDoLoseItem constructor(
        data: MaterialDataCount
): ActionRuleAbstract<MaterialDataCount>(
        "doloseitem",
        DataMaterialDataCount,
        data
)
