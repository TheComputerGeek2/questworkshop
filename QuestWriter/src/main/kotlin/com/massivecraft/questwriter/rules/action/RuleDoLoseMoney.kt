package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.data.DataInt
import com.massivecraft.questwriter.rules.ActionRuleAbstract

/**
 * Removes money from the player.
 */
class RuleDoLoseMoney constructor(
        amount: Int
): ActionRuleAbstract<Int>(
        "dolosemoney",
        DataInt.Companion.AboveZero,
        amount
) {
    var amount: Int
        get() = data
        set(value) { data = value }
}
