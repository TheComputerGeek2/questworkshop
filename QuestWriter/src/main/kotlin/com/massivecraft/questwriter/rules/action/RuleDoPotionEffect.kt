package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.container.PotionEffect
import com.massivecraft.questwriter.data.DataPotionEffect
import com.massivecraft.questwriter.rules.ActionRuleAbstract

/**
 * Applies a potion effect as specified by [com.massivecraft.questwriter.rules.RuleAbstract.data] to the player.
 * @see [PotionEffect]
 */
class RuleDoPotionEffect constructor(
        data: PotionEffect
): ActionRuleAbstract<PotionEffect>(
        "dopotioneffect",
        DataPotionEffect,
        data
) {
    constructor(): this(PotionEffect())
}
