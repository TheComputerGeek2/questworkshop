package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.data.DataString
import com.massivecraft.questwriter.rules.ActionRuleAbstract

class RuleDoRestoreChunk constructor(
        data: String
): ActionRuleAbstract<String>(
        "dorestorechunk",
        DataString,
        data
) // TODO DestinationPsChunk
