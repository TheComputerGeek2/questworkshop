package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.data.DataString
import com.massivecraft.questwriter.rules.ActionRuleAbstract

class RuleDoTeleport constructor(
        data: String
): ActionRuleAbstract<String>(
        "doteleport",
        DataString,
        data
) // TODO Destination
