package com.massivecraft.questwriter.rules.action

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.data.DataNodeId
import com.massivecraft.questwriter.rules.ActionRuleAbstract

/**
 * Unfinds the node specified in [com.massivecraft.questwriter.rules.RuleAbstract.data]
 */
class RuleDoUnfind constructor(
        node: String
): ActionRuleAbstract<String>(
        "dounfind",
        DataNodeId,
        node
) {
    constructor(node: Node): this(node.id!!)
}
