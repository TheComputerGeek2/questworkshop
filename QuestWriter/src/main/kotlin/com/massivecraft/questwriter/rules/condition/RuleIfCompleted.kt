package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.data.DataNodeId
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfCompleted constructor(
        node: String
): ConditionRuleAbstract<String>(
        "ifcompleted",
        DataNodeId,
        node
) {
    constructor(node: Node): this(node.id!!)
}
