package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.data.DataMaterialDataCount
import com.massivecraft.questwriter.data.MaterialDataCount
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfHaveItem constructor(
        data: MaterialDataCount
): ConditionRuleAbstract<MaterialDataCount>(
        "ifhaveitem",
        DataMaterialDataCount,
        data
)
