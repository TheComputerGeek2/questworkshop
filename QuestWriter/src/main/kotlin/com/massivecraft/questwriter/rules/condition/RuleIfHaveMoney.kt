package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.data.DataInt
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfHaveMoney constructor(
        amount: Int
): ConditionRuleAbstract<Int>(
        "ifhavemoney",
        DataInt.Companion.AboveZero,
        amount
) {
    var amount: Int
        get() = data
        set(value) { data = value }
}
