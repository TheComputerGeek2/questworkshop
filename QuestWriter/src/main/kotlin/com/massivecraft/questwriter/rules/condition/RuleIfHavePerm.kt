package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.Permission
import com.massivecraft.questwriter.data.DataPermission
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfHavePerm constructor(
        perm: Permission
): ConditionRuleAbstract<Permission>(
        "ifhaveperm",
        DataPermission,
        perm
)
