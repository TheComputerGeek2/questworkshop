package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.Region
import com.massivecraft.questwriter.data.DataRegion
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfInRegion constructor(
        region: Region
): ConditionRuleAbstract<Region>(
        "ifinregion",
        DataRegion,
        region
)
{
    constructor(): this(Region())
}
