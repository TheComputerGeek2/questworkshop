package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.World
import com.massivecraft.questwriter.data.DataWorld
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfInWorld constructor(
        world: World
): ConditionRuleAbstract<World>(
        "ifinworld",
        DataWorld,
        world
)
