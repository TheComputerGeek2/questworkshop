package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.data.DataMaterialData
import com.massivecraft.questwriter.data.MaterialData
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfNearBlock constructor(
        data: MaterialData
): ConditionRuleAbstract<MaterialData>(
        "ifnearblock",
        DataMaterialData,
        data
)
