package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.data.DataLivingEntityType
import com.massivecraft.questwriter.rules.ConditionRuleAbstract
import org.bukkit.entity.EntityType

class RuleIfNearMob constructor(
        type: EntityType
): ConditionRuleAbstract<EntityType>(
        "ifnearmob",
        DataLivingEntityType,
        type
)
