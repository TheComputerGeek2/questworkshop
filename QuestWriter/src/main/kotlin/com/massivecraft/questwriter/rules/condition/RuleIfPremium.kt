package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.data.DataNone
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfPremium constructor(
): ConditionRuleAbstract<Unit>(
        "ifpremium",
        DataNone,
        Unit
)
