package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.data.DataInt
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfSlotsEmpty constructor(
        data: Int
): ConditionRuleAbstract<Int>(
        "ifslotsempty",
        DataInt.Companion.AboveZero,
        data
) {
    var slots: Int
        get() = data
        set(value) { data = value }
}
