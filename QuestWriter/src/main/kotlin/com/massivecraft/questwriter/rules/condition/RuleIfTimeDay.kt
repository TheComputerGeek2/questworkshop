package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.data.DataNone
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfTimeDay constructor(
): ConditionRuleAbstract<Unit>(
        "iftimeday",
        DataNone,
        Unit
)
