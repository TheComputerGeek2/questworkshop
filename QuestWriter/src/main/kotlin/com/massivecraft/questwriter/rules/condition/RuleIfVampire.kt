package com.massivecraft.questwriter.rules.condition

import com.massivecraft.questwriter.data.DataNone
import com.massivecraft.questwriter.rules.ConditionRuleAbstract

class RuleIfVampire constructor(
): ConditionRuleAbstract<Unit>(
        "ifvampire",
        DataNone,
        Unit
)
