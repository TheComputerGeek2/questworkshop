package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataString
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnChat constructor(
        data: String
): TriggerRuleAbstract<String>(
        "onchat",
        DataString,
        data
) // TODO Lowercase string
{
    var msg: String
        get() = data
        set(value) { data = value }
}
