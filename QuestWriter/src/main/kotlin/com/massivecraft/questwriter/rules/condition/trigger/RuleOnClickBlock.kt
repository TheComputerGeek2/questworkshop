package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataMaterialData
import com.massivecraft.questwriter.data.MaterialData
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnClickBlock constructor(
        data: MaterialData
): TriggerRuleAbstract<MaterialData>(
        "onclickblock",
        DataMaterialData,
        data
)
