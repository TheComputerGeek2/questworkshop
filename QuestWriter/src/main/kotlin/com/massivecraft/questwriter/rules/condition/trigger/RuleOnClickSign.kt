package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.QuestSign
import com.massivecraft.questwriter.data.DataQuestSign
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnClickSign constructor(
        sign: QuestSign
): TriggerRuleAbstract<QuestSign>(
        "onclicksign",
        DataQuestSign,
        sign
)
