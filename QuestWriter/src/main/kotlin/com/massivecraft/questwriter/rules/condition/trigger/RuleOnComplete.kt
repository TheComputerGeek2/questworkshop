package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.data.DataNodeId
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnComplete constructor(
        node: String
): TriggerRuleAbstract<String>(
        "oncomplete",
        DataNodeId,
        node
) {
    constructor(node: Node): this(node.id!!)
}
