package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataMaterialData
import com.massivecraft.questwriter.data.MaterialData
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnConsume constructor(
        data: MaterialData
): TriggerRuleAbstract<MaterialData>(
        "onconsume",
        DataMaterialData,
        data
)
