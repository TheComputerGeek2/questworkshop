package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataMaterialDataCount
import com.massivecraft.questwriter.data.MaterialDataCount
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnCraft constructor(
        data: MaterialDataCount
): TriggerRuleAbstract<MaterialDataCount>(
        "oncraft",
        DataMaterialDataCount,
        data
)
