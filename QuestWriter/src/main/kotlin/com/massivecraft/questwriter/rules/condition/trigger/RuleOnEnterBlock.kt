package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataMaterialData
import com.massivecraft.questwriter.data.MaterialData
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnEnterBlock constructor(
        data: MaterialData
): TriggerRuleAbstract<MaterialData>(
        "onenterblock",
        DataMaterialData,
        data
)
