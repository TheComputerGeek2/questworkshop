package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.World
import com.massivecraft.questwriter.data.DataWorld
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnEnterWorld constructor(
        world: World
): TriggerRuleAbstract<World>(
        "onenterworld",
        DataWorld,
        world
)
