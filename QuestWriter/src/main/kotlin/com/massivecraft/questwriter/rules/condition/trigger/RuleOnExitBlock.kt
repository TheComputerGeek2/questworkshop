package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataMaterialData
import com.massivecraft.questwriter.data.MaterialData
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnExitBlock constructor(
        data: MaterialData
): TriggerRuleAbstract<MaterialData>(
        "onexitblock",
        DataMaterialData,
        data
)
