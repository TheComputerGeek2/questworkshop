package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.Region
import com.massivecraft.questwriter.data.DataRegion
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnExitRegion constructor(
        region: Region
): TriggerRuleAbstract<Region>(
        "onexitregion",
        DataRegion,
        region
) {
    constructor(): this(Region())
}
