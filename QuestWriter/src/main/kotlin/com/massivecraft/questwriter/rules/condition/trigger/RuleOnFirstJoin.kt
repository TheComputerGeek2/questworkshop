package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataNone
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnFirstJoin constructor(
): TriggerRuleAbstract<Unit>(
        "onfirstjoin",
        DataNone,
        Unit
)
