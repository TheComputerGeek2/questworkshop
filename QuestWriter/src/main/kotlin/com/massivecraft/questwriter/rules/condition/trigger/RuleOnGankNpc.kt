package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.Npc
import com.massivecraft.questwriter.data.DataNpc
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnGankNpc constructor(
        npc: Npc
): TriggerRuleAbstract<Npc>(
        "onganknpc",
        DataNpc,
        npc
) {
    constructor(): this(Npc())
}
