package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataLivingEntityType
import com.massivecraft.questwriter.rules.TriggerRuleAbstract
import org.bukkit.entity.EntityType

class RuleOnImpactMob constructor(
        type: EntityType
): TriggerRuleAbstract<EntityType>(
        "onimpactmob",
        DataLivingEntityType,
        type
)
