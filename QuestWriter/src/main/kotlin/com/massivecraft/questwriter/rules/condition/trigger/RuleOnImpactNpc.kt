package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.Npc
import com.massivecraft.questwriter.data.DataNpc
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnImpactNpc constructor(
        npc: Npc
): TriggerRuleAbstract<Npc>(
        "onimpactnpc",
        DataNpc,
        npc
) {
    constructor(): this(Npc())
}
