package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.Region
import com.massivecraft.questwriter.data.DataRegion
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnImpactRegion constructor(
        region: Region
): TriggerRuleAbstract<Region>(
        "onimpactregion",
        DataRegion,
        region
) {
    constructor(): this(Region())
}
