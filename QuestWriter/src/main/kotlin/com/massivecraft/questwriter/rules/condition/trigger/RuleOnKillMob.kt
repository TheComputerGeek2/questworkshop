package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataLivingEntityType
import com.massivecraft.questwriter.rules.TriggerRuleAbstract
import org.bukkit.entity.EntityType

class RuleOnKillMob constructor(
        type: EntityType
): TriggerRuleAbstract<EntityType>(
        "onkillmob",
        DataLivingEntityType,
        type
)
