package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.Npc
import com.massivecraft.questwriter.data.DataNpc
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnKillNpc constructor(
        npc: Npc
): TriggerRuleAbstract<Npc>(
        "onkillnpc",
        DataNpc,
        npc
) {
    constructor(): this(Npc())
}
