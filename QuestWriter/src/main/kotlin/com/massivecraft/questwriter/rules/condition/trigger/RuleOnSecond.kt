package com.massivecraft.questwriter.rules.condition.trigger

import com.massivecraft.questwriter.data.DataInt
import com.massivecraft.questwriter.rules.TriggerRuleAbstract

class RuleOnSecond constructor(
        interval: Int
): TriggerRuleAbstract<Int>(
        "onsecond",
        DataInt.Companion.AboveZero,
        interval
)
