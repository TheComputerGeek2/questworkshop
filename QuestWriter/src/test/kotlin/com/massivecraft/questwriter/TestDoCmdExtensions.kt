package com.massivecraft.questwriter

import com.massivecraft.questwriter.dsl.onComplete
import com.massivecraft.questwriter.dsl.rule.unfindTree
import com.massivecraft.questwriter.quest.child
import com.massivecraft.questwriter.quest.stringify
import kotlin.test.Test
import kotlin.test.assertEquals

class TestDoCmdExtensions
{
    @Test
    fun `unfind tree`()
    {
        val rootNode = Node { id = "some.node.id" }
        val childNode: Node = rootNode.child("child")
        childNode.onComplete {
            unfindTree(rootNode)
        }

        assertEquals(arrayListOf("/q new some.node.id"), rootNode.stringify())
        assertEquals(arrayListOf(
                "/q new some.node.id.child",
                "/q e oncomplete docmd /q p ut {p} some.node.id"
        ), childNode.stringify())
    }

}
