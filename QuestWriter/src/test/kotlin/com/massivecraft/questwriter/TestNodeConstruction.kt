package com.massivecraft.questwriter

import com.massivecraft.questwriter.quest.child
import kotlin.test.Test
import kotlin.test.assertEquals

class TestNodeConstruction
{
    @Test
    fun `child extension function`()
    {
        val parent = Node { id = "some.quest.node" }
        val child: Node = parent.child("child")
        val commands: List<String> = StringifierNode.stringify(child)
        assertEquals(arrayListOf("/q new some.quest.node.child"), commands)
    }

}
