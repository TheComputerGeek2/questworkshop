package com.massivecraft.questwriter

import com.massivecraft.questwriter.dsl.rule.onChat
import com.massivecraft.questwriter.dsl.toFind
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails

class TestNodePrinting
{
    @Test
    fun `basic node printing`()
    {
        val node = Node {
            id = "some.node.id"
            name = "test name"
            description = "Some node description"
        }
        val commands: List<String> = StringifierNode.stringify(node)
        assertEquals(arrayListOf(
                "/q new some.node.id",
                "/q e name test name",
                "/q e desc Some node description"
        ), commands)
    }

    @Test
    fun `throws error with no id`()
    {
        val node = Node {
            name = "this should not be reached"
        }
        assertFails {
            StringifierNode.stringify(node)
        }
    }

    @Test
    fun `node with rule lines`()
    {
        val node = Node {
            id = "some.node.id"
            toFind {
                onChat("hello")
            }
        }
        val commands: List<String> = StringifierNode.stringify(node)
        assertEquals(arrayListOf(
                "/q new some.node.id",
                "/q e tofind onchat hello"
        ), commands)
    }

}
