package com.massivecraft.questwriter

import com.massivecraft.questwriter.quest.nodes
import com.massivecraft.questwriter.quest.quest
import kotlin.test.Test

class TestQuest
{
    @Test
    fun `test quest`()
    {
        val quest = quest {
            // TODO setup npcs and such
            // Nodes
            nodes {
                val root = node {
                    id = "some.node.id"
                }
                node {
                    id = "${root.id}.child"
                }
            }
        }

        quest.backout().forEach { println(it) }
        quest.implementation().forEach { println(it) }
    }

}
