package com.massivecraft.questwriter

import com.massivecraft.questwriter.quest.backout
import com.massivecraft.questwriter.quest.stringify
import kotlin.test.Test
import kotlin.test.assertEquals

class TestQuestPrinting
{
    @Test
    fun `print collection of nodes`()
    {
        val nodes = arrayListOf(
                Node { id = "some.node.id" },
                Node { id = "some.node.id.child" },
                Node { id = "some.node.id.child2" }
        )

        val expected = arrayListOf(
                "/q new some.node.id",
                "/q new some.node.id.child",
                "/q new some.node.id.child2"
        )
        val actual = nodes.stringify()
        assertEquals(expected, actual)
    }

    @Test
    fun `quest backout codes`()
    {
        val nodes = arrayListOf(
                Node { id = "some.node.id" },
                Node { id = "some.node.id.child" },
                Node { id = "some.node.id.child2" }
        )
        val expected = arrayListOf(
                "/q delete some.node.id",
                "/q delete some.node.id.child",
                "/q delete some.node.id.child2"
        )
        val actual = nodes.backout()
        assertEquals(expected, actual)
    }

}
