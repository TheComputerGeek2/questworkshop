package com.massivecraft.questwriter

import com.massivecraft.questwriter.rules.condition.trigger.RuleOnChat
import kotlin.test.Test
import kotlin.test.assertEquals

class TestRulePrints
{
    @Test
    fun testPrint()
    {
        assertEquals(
                "!onchat test",
                RuleOnChat("test").apply { wanted = false }.ruleLine()
        )
    }

}
