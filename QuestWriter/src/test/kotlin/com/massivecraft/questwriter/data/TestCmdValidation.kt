package com.massivecraft.questwriter.data

import kotlin.test.Test
import kotlin.test.assertFails

class TestCmdValidation
{
    @Test
    fun `require leading slash`()
    {
        assertFails { DataStringCommand.acceptData("say this should fail") }

        // This should not fail
        DataStringCommand.acceptData("/say this should not fail")
    }

    @Test
    fun `require not blank`()
    {
        assertFails("shold fail with whitespace only data") { DataStringCommand.acceptData("  ") }

        assertFails("should fail with empty string") { DataStringCommand.acceptData("") }
    }

}
