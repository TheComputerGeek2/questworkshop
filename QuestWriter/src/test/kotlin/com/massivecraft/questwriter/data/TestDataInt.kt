package com.massivecraft.questwriter.data

import kotlin.test.Test
import kotlin.test.assertFails

class TestDataInt
{
    @Test
    fun `above zero`()
    {
        // Should accept
        DataInt.Companion.AboveZero.acceptData(1)

        assertFails { DataInt.Companion.AboveZero.acceptData(0) }
        assertFails { DataInt.Companion.AboveZero.acceptData(-1) }
    }

    @Test
    fun any()
    {
        DataInt.Companion.Any.acceptData(1)
        DataInt.Companion.Any.acceptData(0)
        DataInt.Companion.Any.acceptData(-1)
    }

}
