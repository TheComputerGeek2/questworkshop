package com.massivecraft.questwriter.data

import com.massivecraft.questwriter.Region
import kotlin.test.Test
import kotlin.test.assertEquals

class TestDataRegion
{
    @Test
    fun `data region print format`()
    {
        val region = Region(world = "event", regionName = "icepve")
        val cmdString = DataRegion.dataToCommandUsable(region)
        assertEquals("event.icepve", cmdString)
    }

}
