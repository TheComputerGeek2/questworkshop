package com.massivecraft.questwriter.dsl

import com.massivecraft.questwriter.Node
import com.massivecraft.questwriter.dsl.rule.doCmd
import com.massivecraft.questwriter.dsl.rule.doPotionEffect
import com.massivecraft.questwriter.dsl.rule.effect
import com.massivecraft.questwriter.dsl.rule.ifNotInRegion
import com.massivecraft.questwriter.dsl.rule.region
import com.massivecraft.questwriter.quest.stringify
import com.massivecraft.questwriter.rules.action.RuleDoGiveMoney
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnChat
import com.massivecraft.questwriter.rules.condition.trigger.RuleOnFirstJoin
import kotlin.test.Test
import kotlin.test.assertEquals

class TestDSLNode
{
    @Test
    fun `node dsl`()
    {
        val node = Node {
            id = "some.node.id"
            toFind {
                add(RuleOnFirstJoin())
            }
            toAdvance {
                ifNotInRegion {
                    region {
                        world = "lobby"
                        regionName = "botcatch"
                    }
                }
                add(RuleOnChat(data = "hello!"))
            }
            onFind {
                doPotionEffect {
                    effect {
                        type = "ABSORPTION"
                        amplifier = 1
                        duration = 1000
                        ambient = false
                        particles = true
                    }
                }
            }
            onAdvance {
                add(RuleDoGiveMoney(amount = 10))
            }
            onComplete {
                add(RuleDoGiveMoney(amount = 100))
                doCmd {
                    cmd = "/q p ut {p} some.node.id"
                }
            }
        }
        val expected = arrayListOf(
                "/q new some.node.id",
                "/q e tofind onfirstjoin",
                "/q e toadvance !ifinregion lobby.botcatch",
                "/q e toadvance onchat hello!",
                "/q e onfind dopotioneffect ABSORPTION 1 1000 false true",
                "/q e onadvance dogivemoney 10",
                "/q e oncomplete dogivemoney 100",
                "/q e oncomplete docmd /q p ut {p} some.node.id"
        )
        val commands = node.stringify()
        assertEquals(expected = expected, actual = commands)
    }

}
