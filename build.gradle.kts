import java.net.URI

description = "Well, more like a workshop workshop"

tasks.wrapper {
    distributionType = Wrapper.DistributionType.ALL
    gradleVersion = "5.3"
}

allprojects {
    repositories {
        jcenter()
        maven { url = URI("https://oss.sonatype.org/content/repositories/snapshots") }
        maven { url = URI("https://hub.spigotmc.org/nexus/content/repositories/snapshots/") }
    }
    group = "org.bitbucket.TheComputerGeek2"
    version = "1.0-SNAPSHOT"
}

buildscript {
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.21")
    }
}

fun shouldPublish(project: Project): Boolean {
    println(project.name)
    // TODO implement this
    return true
}

subprojects {
    apply(plugin = "java")
    apply(plugin = "kotlin")
    apply(plugin = "maven-publish")
    /*repositories {
        maven {
            url = URI("http://repo.maven.apache.org/maven2")
        }
    }*/
    if (shouldPublish(this)) {
        apply(from = "$rootDir/publishing.build.gradle")
    }
}
