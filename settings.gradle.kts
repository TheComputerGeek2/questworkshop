rootProject.name = "QuestWorkshop"

include(":QuestWriter")
include(":Examples")
include(":CommandProvider")
include(":Providers:MassiveBasicCommands")
include(":Providers")
project(":Providers:MassiveBasicCommands").projectDir = file("Providers/MassiveBasicCommands")
include(":NodesToCommands")

include(":QuestGradle")

startParameter.isParallelProjectExecutionEnabled = true
startParameter.isBuildCacheEnabled = true
buildCache {
    local(DirectoryBuildCache::class) {
        directory = "$rootDir/buildcache"
        isEnabled = true
        isPush = true
    }
}
